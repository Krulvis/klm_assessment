# KLM_XRCOE_Opdracht_2021

## GameManager:

GameManager is made globally accessible by creating a singleton instance. It manages the planes and hangars by
collecting the hangars created in Unity and creating airplanes at start. It assigns numbers to the hangars according to
their index in the collection. <br>
It manages the global events which are called by the UI Canvas Buttons either directly or by subscribing custom events
to the onClick listeners.

## AirplaneBehavior:

This MonoBehavior class is used to control the plane. It contains the `NavMeshAgent`, and `AirplaneScriptableObject`.
Additionally it contains a static function Create() that can be used to easily instantiate new planes from another
script (such as GameManager). Create() automatically loads the edited `Airplane` prefab using pre-loaded resources
from `GameManager`. <br>
Additionally, AirplaneBehavior rolls/tilts the plane slightly while rotating to another destination. It subscribes to
the events from `GameManager` on create. Finally, I was working on writing a custom NavMesh agent pathfinder that uses
forward driven motion (instead of the default NavMeshAgent that separates rotation and acceleration) but figured that
this was outside the scope of this assessment.

### Unity Setup

#### Main scene

The `SpawnArea` in `Environment/Ground` is used in `GameManager` script to be able to generate random points within the
walkable area. The NavMesh itself is baked on the full `Ground` as walkable area and `Buildings` and `Fences` as
unwalkable objects.

https://www.youtube.com/watch?v=FyK36gpe23U

#### Demo scene

Similar to the Main scene except just way more basic.

https://www.youtube.com/watch?v=XaDu-5K3xXw


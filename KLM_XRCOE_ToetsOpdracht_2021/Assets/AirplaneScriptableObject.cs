using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Airplane", menuName = "ScriptableObject/Airplane", order = 1)]
public class AirplaneScriptableObject : ScriptableObject
{
    public string brand;
    public string model;
}
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    //Used as singleton Instance
    private static GameManager _gm;

    /// <summary>
    /// Event to which AirplaneBehavior subscribes to park 
    /// </summary>
    public static event Action OnParkEvent;

    /// <summary>
    /// Event to which AirplaneBehavior subscribes to switch on/off lights 
    /// </summary>
    public static event Action OnLightsEvent;

    public GameObject spawnArea;
    public GameObject hangarsParent;
    public GameObject globalLight;
    public GameObject mainCamera;
    public bool wander;
    public bool randomPlaneData = false;
    public Button parkButton;
    public Button lightsButton;

    public float floorLevel = 0.1f;
    public float lightIntensity = 1f;
    private List<GameObject> airplanes = new List<GameObject>();
    private List<GameObject> hangars = new List<GameObject>();

    /// <summary>
    /// Allow GameManager to be accessible throughout other script by providing a singleton instance
    /// </summary>
    public static GameManager Instance
    {
        get
        {
            if (_gm == null) Debug.LogError("GameManager instance not yet initialized.");
            return _gm;
        }
    }

    private void Awake()
    {
        //Set _gm
        _gm = this;

        //Register events to button onClick listener
        //I have added the functions for two buttons in the editor (which is what you would do normally)
        //But I want to show that you can also add functions as listener to Button's onClick like this
        parkButton.onClick.AddListener(() => OnParkEvent.Invoke());
        lightsButton.onClick.AddListener(() => OnLightsEvent.Invoke());
        lightsButton.onClick.AddListener(SwitchGlobalLight);

        //Load resources to init airplanes (alternative way of creating GameObjects)
        var airplaneScriptables = Resources.LoadAll<AirplaneScriptableObject>("ScriptableAirplanes");
        var airplanePrefab = Resources.Load("Prefabs/Airplane");

        //Initiate the Hangars & Create an airplane for each
        for (int i = 0; i < hangarsParent.transform.childCount; i++)
        {
            var hangar = hangarsParent.transform.GetChild(i).gameObject;
            hangars.Add(hangar);
            //I show here how you can dynamically create GameObjects using a custom constructor. I could have done it the same way as the hangars (creating objects from ScriptableObjects in Unity editor)
            //but I want to show an alternative way.
            var airplaneData = randomPlaneData
                ? airplaneScriptables[Random.Range(0, airplaneScriptables.Length)]
                : airplaneScriptables[i];
            airplanes.Add(AirplaneBehavior.Create(airplanePrefab, airplaneData, hangar, mainCamera));
        }
    }

    /// <summary>
    /// Associates a hangar with the plane and calls Initialize on PlaneBehavior
    /// Number of planes should be equal to the number of hangars
    /// calls ResetHangars();
    /// </summary>
    public void SpawnPlanes()
    {
        // Make sure we have an equal number of planes & hangars
        Assert.IsTrue(airplanes.Count == hangars.Count, "airplanes.Length == hangars.Length");

        //Reset environment
        wander = false;
        ResetHangars();
        globalLight.GetComponent<Light>().intensity = lightIntensity;

        //Respawn planes
        for (var i = 0; i < airplanes.Count; i++)
        {
            var plane = airplanes[i];
            plane.SetActive(true);
            plane.GetComponent<AirplaneBehavior>().Respawn();
        }
    }

    /// <summary>
    /// Resets the text on the hangars according to their index in the array
    /// </summary>
    private void ResetHangars()
    {
        for (var i = 0; i < hangars.Count; i++)
        {
            hangars[i].GetComponentInChildren<TextMeshPro>().SetText(i.ToString());
            print($"Reset text of Hanger={hangars[i]} to={i.ToString()}");
        }
    }

    /// <summary>
    /// Creates and returns a randomly generated Vector3 within the bounds of the [spawnArea] GameObject
    /// </summary>
    /// <returns>Random Vector3 point within the bounds of [spawnArea]</returns>
    public Vector3 RandomSpawnPoint()
    {
        var bounds = spawnArea.GetComponent<Renderer>().bounds;
        return new Vector3(
            Random.Range(bounds.min.x, bounds.max.x),
            floorLevel,
            Random.Range(bounds.min.z, bounds.max.z)
        );
    }

    /// <summary>
    /// Wander is accessed from AirplaneBehavior to determine whether or not the airplanes should be wandering around
    /// Could also be done through an event but this is just to show another way of handling UI buttons
    /// </summary>
    public void SwitchWander()
    {
        wander = !wander;
    }

    /// <summary>
    /// Switches light on depending on whether or not first plane has its lights turned on.
    /// This function gets added after the planes switch on their light so it will use the updated plane's light state.
    /// </summary>
    private void SwitchGlobalLight()
    {
        if (airplanes.Count > 0)
        {
            var night = airplanes[0].GetComponentInChildren<Light>().enabled;
            globalLight.GetComponent<Light>().intensity = night ? 0.4f : lightIntensity;
        }
    }
}
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using TMPro;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

[RequireComponent(typeof(NavMeshAgent))]
public class AirplaneBehavior : MonoBehaviour
{
    public AirplaneScriptableObject airplaneData;
    private GameObject m_Camera;

    private NavMeshAgent agent;
    private Vector3 hangarLocation;
    private Vector3? destination;
    private GameObject hangar;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        //Subscribe to all events
        GameManager.OnParkEvent += Park;
        GameManager.OnLightsEvent += SwitchLights;
        name = $"{airplaneData.brand}: {airplaneData.model}";
    }


    private void Update()
    {
        //If the GameManager has wander flag enabled, the plane needs to find random new destinations to travel to
        if (GameManager.Instance.wander)
        {
            SetWanderDestination();
        }

        //Set new destination to agent.
        //I want to keep track of the destination using a separate variable (outside of Agent) because the agent.destination changes slightly sometimes
        if (destination == null)
        {
            agent.SetDestination(agent.transform.position);
            return;
        }

        //From here on we know that we have a destination
        agent.SetDestination(destination.Value);
        Roll(); //Far enough from destination to add some roll action
        TextFaceCamera(); // Have the text face the camera

        //We have arrived at the hangar, notifying hangar and disabling plane.
        if (destination == hangarLocation && Vector3.Distance(transform.position, agent.destination) <= 1f)
        {
            print($"{name} Reached hangar!");
            hangar.GetComponentInChildren<TextMeshPro>().SetText("\uf00c");
            print($"Airplane text of Hanger={hangar} to={"\uf00c"}");
            SetLights(false);
            gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Initializes all values of the plane. Cannot be done in Awake as GameManager might still be null at that point
    /// </summary>
    /// <param name="hangerGameObject">Hangar GameObject to which this plane belongs</param>
    public void Respawn()
    {
        //Set text according to hangar number
        GetComponentInChildren<TextMeshPro>().SetText(hangar.GetComponentInChildren<TextMeshPro>().text);

        //Turn lights off
        SetLights(false);

        //Reset destination and give random position / rotation
        transform.position = GameManager.Instance.RandomSpawnPoint();
        transform.rotation = Random.rotation;
        destination = null;

        //Let text face the camera
        TextFaceCamera();
    }

    /// <summary>
    /// Makes the Airplane wander around. Gives it's NavMeshAgent a new destination if too close to previous one
    /// </summary>
    private void SetWanderDestination()
    {
        if (destination != hangarLocation &&
            //Whenever the current destination is null or we are close to the last destination, set new one
            (destination == null || Vector3.Distance(transform.position, agent.destination) <= 0.1f))
        {
            Vector3 newPos = GameManager.Instance.RandomSpawnPoint();
            //Generate a point within the NavMesh to go to.
            NavMesh.SamplePosition(newPos, out var hit, Mathf.Infinity, NavMesh.AllAreas);
            destination = hit.position;

            // print($"{name} new wander destination: {destination}");
        }
    }

    /// <summary>
    /// Roll the plane along with the horizontal movement
    /// Translate the horizontal SignedAngle to a z-axis rotation between -40 and 40 degrees. (No overtilt) 
    /// </summary>
    private void Roll()
    {
        var direction = agent.steeringTarget - transform.position;
        if (direction.x <= 0.2f && direction.z <= 0.2f) return; //Not moving enough to roll
        var angle = -1 * Vector3.SignedAngle(transform.forward, direction, Vector3.up);
        // print($"Direction: {direction} Velocity: {agent.velocity.magnitude}, angle: {angle}");
        // print($"x={direction.x} ({direction.x == 0.0f}), y={direction.y}, z={direction.z}");
        agent.transform.Rotate(0, 0f, Math.Max(-40f, Math.Min(40f, angle)));
    }

    /// <summary>
    /// Make sure that the TextMeshPro child always faces the camera
    /// </summary>
    private void TextFaceCamera()
    {
        GetComponentInChildren<TextMeshPro>().transform
            .LookAt(transform.position + m_Camera.transform.rotation * Vector3.forward,
                m_Camera.transform.rotation * Vector3.up);
    }

    /// <summary>
    /// Flies the plane to its starting location (Inside the Hangar)
    /// </summary>
    private void Park()
    {
        print($"Flying {name} home to: {hangarLocation}");
        destination = hangarLocation;
    }

    /// <summary>
    /// Sets lights on / off depending on whether or not they currently are on
    /// </summary>
    private void SwitchLights()
    {
        var planeLights = GetComponentsInChildren<Light>();
        //In the SwitchLights & SetLights I show how both a regular for-loop and lambda expressions as Action delegate in List.ForEach
        foreach (var planeLight in planeLights)
        {
            planeLight.enabled = !planeLight.enabled;
        }
    }

    /// <summary>
    /// Sets lights on/off using an [on] parameter
    /// </summary>
    /// <param name="on">Whether or not the lights should be turned on</param>
    private void SetLights(bool on)
    {
        var planeLights = GetComponentsInChildren<Light>().ToList();
        planeLights.ForEach(planeLight => planeLight.enabled = on);
    }

    /// <summary>
    /// Create an Airplane using a static method, needs prefab and data to instantiate
    /// </summary>
    /// <param name="prefab">Airplane prefab to give to the airplane</param>
    /// <param name="data">AirplaneScriptObject data that this plane will use</param>
    /// <returns></returns>
    public static GameObject Create(
        Object prefab,
        AirplaneScriptableObject data,
        GameObject hangerGameObject,
        GameObject mainCamera
    )
    {
        var plane = Instantiate(prefab) as GameObject;
        var behavior = plane.GetComponent<AirplaneBehavior>();
        behavior.airplaneData = data;
        behavior.m_Camera = mainCamera;

        //Associate with hangerGameObject
        behavior.hangar = hangerGameObject;
        behavior.hangarLocation = hangerGameObject.GetComponentInChildren<Renderer>().bounds.center;
        behavior.hangarLocation.y = GameManager.Instance.floorLevel;

        //Init in hangar such that the walkable area is always on the floor
        plane.transform.position = behavior.hangarLocation;
        return plane;
    }
}